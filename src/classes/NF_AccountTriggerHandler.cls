/**
* @File Name:   NF_AccountTriggerHandler.cls
* @Description:   
* @Author:      Sharath Chandra
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2018-06-10  sharath chandra    Created the file/class
*/
public with sharing class NF_AccountTriggerHandler extends NF_AbstractTriggerHandler {
    public override void beforeUpdate(){

    }

    public override void afterUpdate(){

    }

    public override void beforeInsert(){

    }

    public override void afterInsert(){
    List<Contact> newContacts = new List<Contact>();
    for(Account newAccount : (List<Account>)Trigger.new){
      newContacts.add(new Contact(AccountId = newAccount.id,LastName=newAccount.Name));
    }
    if(newContacts.size() > 0){
      insert newContacts;
      system.debug('ContactID******'+ newContacts);
    }

    }

    public override void afterDelete(){

    }

    public override void andFinally(){

    }
}