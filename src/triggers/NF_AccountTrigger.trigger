/**
* @File Name:   NF_AccountTrigger.trigger
* @Description: Trigger for Assignment 
* @Author:      Sharath Chandra
* @Group:       Trigger
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2018-06-10  Sharath Chandra    Created the file/class
*/
trigger NF_AccountTrigger on Account (after insert) {
    NF_TriggerFactory.CreateHandlerAndExecute(Account.sObjectType);
}
